# codeing = utf-8
# -*- coding:utf-8 -*-
# Author：直言
import pymysql
import redis
import urllib3
import traceback
import json

def mysqlexec(sqltext):

    conn = pymysql.connect(host='172.17.0.136', port=3306, user='dev_rr', passwd='527UsBPwlwY8yDrPn78H',
                           db='kxx_xxk_pro', charset='utf8')

    # conn = pymysql.connect(host='172.17.0.136', port=3306, user='dev_rr', passwd='527UsBPwlwY8yDrPn78H',
    #                        db='kxx_xxk_pro', charset='utf8')

    row_data = ''

    try:
        cursor = conn.cursor()
        # print('mysql开始执行：',sqltext)
        effect_row = cursor.execute(sqltext)
        conn.commit()
        if effect_row == 0:
            print('sql执行无数据:',sqltext)
            return []

        #row_data = cursor.fetchone()
        datalist = []
        while effect_row > 0:
            effect_row = effect_row - 1
            row_1 = cursor.fetchone()
            datalist.append(row_1)

        # print('mysql执行结果datalist===：',datalist)
    except Exception as e:
        print(e)
        return []
    finally:
        conn.close()

    return datalist



def httpRequest(method,url,data,header):
    urllib3.disable_warnings()
    http = urllib3.PoolManager()
    print('http请求 开始------------------------------------------------------------------------------------------------------------')
    print(method,'\t',url)
    print(data)
    recv = ''
    if str.upper(method) == 'POST':
        recv = http.request("POST", url, body=data.encode(), headers=header, timeout=30)
    if str.upper(method) == 'GET':
        recv = http.request("GET", url, body=data.encode(), headers=header, timeout=30)
    print(recv.status)
    try:
        print('http请求结果: ', recv.data.decode())
    except:
        print('http请求结果: ', recv.data)
    print('http请求 结束------------------------------------------------------------------------------------------------------------')
    return recv


def redisexec(method,key,value):
    res = ''
    try:
        r = redis.Redis(host='r-2zeocvjcu9w50eg8ki.redis.rds.aliyuncs.com', port=6379, decode_responses=True)
        if str.upper(method) == 'GET':

            return r.get(key)
        if str.upper(method) == 'SET':
            return r.set(key,value)
    except:
        res = None
    return res

def redisEXPIRE(key,ttl):
    res = ''
    try:
        r = redis.Redis(host='r-2zeocvjcu9w50eg8ki.redis.rds.aliyuncs.com', port=6379, decode_responses=True)
        r.expire(key,ttl)
    except:
        res = None
    return res


def getTokenByPhone(phone):
    token = ''
    # uuid = mysqlexec("select member_uuid from kxx_pro.ums_member where phone = '%s'" % phone)
    # print('uuid====',uuid[0][0])
    uuid = '159084'
    if uuid != None and len(uuid) > 0:
        # key = 'i_kxx-auth:jwt-token:%s:PTAPP'% uuid[0][0]
        key = 'i_kxx-auth:jwt-token:159084:PTAPP'
        print('key===',key)
        token = redisexec('get',key,'')
        print('token===',token)
        return token
    return token

def alarm_workwechat(msg):
    error_count = 0
    header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json"}
    if msg == None:
        return 1

    alarm_count = redisexec('get', 'alarm_count', '')
    if alarm_count == None:
        msg = msg.replace("\"", "'")
        alarm_msg = r'{ "msgtype": "text", "text": { "content": "%s" } }' % msg
        httpRequest('POST',
                          'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=872cb4cc-1f29-41c6-8a7c-21e8c65d94d5',
                          alarm_msg, header)
        error_count += 1


    if alarm_count != None:
        error_count += int(alarm_count)
    if alarm_count != None and int(alarm_count) < 5:
        if error_count >= 4:
            msg = '''%s\n备注: 1小时内将不在提醒 ''' %msg

        msg = msg.replace("\"", "'")
        alarm_msg = r'''{ "msgtype": "text", "text": { "content": "%s" } }''' % msg

        res = httpRequest('POST',
                          'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=872cb4cc-1f29-41c6-8a7c-21e8c65d94d5',
                          alarm_msg, header)
        error_count += 1

    if error_count > 0:
        redisexec('set', 'alarm_count', error_count)
        redisEXPIRE('alarm_count', 3600)




hostname = "https://test428.kuxiaoxiao.com"
hostname = 'https://zpkxi.kuxiaoxiao.com'
token = getTokenByPhone('13401157249')

key = 'test-token'
if token != None and len(token) > 10:
    redisexec('set', key, token)
if token == None:
    token = redisexec('get',key,'')

request_list = [
    {"method":"POST","url":"/api/group/product/getProduct","data":r'{"pageNum":"1","pageSize":"10"}'},
    {"method":"POST","url":"/api/auth/jwt/token2","data":r'{ "phone" : "12000000000", "os" : "1", "invitationCode" : "", "ua" : "test-UA", "code" : "000000", "type" : "3", "idfa" : "2EB1259B-0228-4490-914F-E334594897A7" }'},
    {"method":"GET","url":"/api/group/asset/getDetailPage?crossType=0&current=1&size=10&type=3","data":""},
    {"method":"GET","url":"/api/group/user/invitation/getInvitationAndExtensionList?current=1&orderType=1&size=20&type=1","data":""}
]

error_count = 0
msg = None
alarm_msg = None
for request_data in request_list:
    msg = None
    alarm_msg = None

    # token = getTokenByPhone('13401157249')
    header = {'User-Agent': 'QA-yujianyu', "Content-Type": "application/json",
              "Authorization": "%s" % token}
    try:
        res = httpRequest(request_data["method"], '%s%s' % (hostname, request_data["url"]), request_data["data"], header)
        print('----------------------------')
        print(res.status)
        # print(res.data)

        if res == None:
            print('未获取到响应结果:', request_data["url"])

        if res.status != 200:
            msg = r"异常接口:%s \n 响应码:%s \n%s" % (request_data["url"], res.status, res.data.decode())
            # alarm_msg = r'{ "msgtype": "text", "text": { "content": "%s" } }' % msg
            print('响应结果:', request_data["url"])

        if res.status == 200 and json.loads(res.data.decode()).get("code") != 200 and (request_data["url"] != '/api/auth/jwt/token2'):
            msg = r"异常接口:%s \n 响应码:%s \n%s" % (request_data["url"], res.status, res.data.decode())
            # alarm_msg = r'{ "msgtype": "text", "text": { "content": "%s" } }' % msg
            print('响应结果:', request_data["url"])
    except:
        print('未获取到响应结果:',request_data["url"])
        print(traceback.format_exc())
        msg = '未获取到响应结果:%s'% request_data["url"]

    alarm_workwechat(msg)
